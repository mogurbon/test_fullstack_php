<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#use MarkSitko\LaravelUnsplash\Unsplash;
#use MarkSitko\LaravelUnsplash\UnsplashServiceProvider;
use MarkSitko\LaravelUnsplash\UnsplashFacade as Unsplash;

class PropertyController extends Controller
{
    public function index(){
        $twoRandomPhotosOfSomePeoples = Unsplash::randomPhoto()
            ->orientation('portrait')
            ->term('people')
            ->count(2)
            ->toJson();
       # dd($twoRandomPhotosOfSomePeoples);
        return view('content.add');
    }
}
